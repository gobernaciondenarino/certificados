/*
SQLyog Enterprise v13.1.1 (64 bit)
MySQL - 10.1.41-MariaDB-0+deb9u1 : Database - cpe
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cpe` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `cpe`;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`parent_id`,`order`,`name`,`slug`,`created_at`,`updated_at`) values 
(1,NULL,1,'Category 1','category-1','2019-05-16 20:47:14','2019-05-16 20:47:14'),
(2,NULL,1,'Category 2','category-2','2019-05-16 20:47:14','2019-05-16 20:47:14');

/*Table structure for table `certificates` */

DROP TABLE IF EXISTS `certificates`;

CREATE TABLE `certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identified` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=941 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `certificates` */

insert  into `certificates`(`id`,`name`,`email`,`identified`,`created_at`,`updated_at`,`deleted_at`,`topic_id`) values 
(553,'DIEGO GERMÁN BASANTE NOGUERA','dgbasante@iucesmag.edu.co','10290314',NULL,NULL,NULL,1),
(554,'MARÍA CAMILA ROSALES ROMERO','mariacamila75@gmail.com','1085321587',NULL,NULL,NULL,1),
(555,'CARMEN NILSA ISLENY TORO TORO','cnitt96@gmail.com','1126456037',NULL,NULL,NULL,1),
(556,'CLAUDIA ANDREA GUERRERO MARTÍNEZ','claudiague08@gmail.com','1085262976',NULL,NULL,NULL,1),
(557,'MARIA DEL CARMEN PORTILLA OLIVA','caraiba6927@gmail.com','59817369',NULL,NULL,NULL,1),
(558,'CARLOS ERNESTO DÍAZ BASANTE','carlodiaz@umariana.edu.co','1082749257',NULL,NULL,NULL,1),
(559,'CAMILO SOLARTE','camsolarte@umariana.edu.co','1085343843',NULL,NULL,NULL,1),
(560,'VÍCTOR ANDRÉS JOJOA PRADO ','vaj96@hotmail.com','1085332017',NULL,NULL,NULL,1),
(561,'KAREN VANESSA ARTEAGA GARZON','vanegarzon87@gmail.com','1010016105',NULL,NULL,NULL,1),
(562,'MARÍA MERCEDES TULCAN CABRERA','maria.tulcanc@campusucc.edu.co','1085252662',NULL,NULL,NULL,1),
(563,'LUIS ANTONIO MUESES CORAL','lmueses@hosdenar.gov.co','79592259',NULL,NULL,NULL,1),
(564,'MARILIN ELIZABETH AMAGUAÑA MORENO','97m.eliza@gmail.com','1087961108',NULL,NULL,NULL,1),
(565,'ALVARO CAMILO MUÑOZ MORALES','acmunoz@umariana.edu.co','1085264028',NULL,NULL,NULL,1),
(566,'PAOLA ANDREA ROSERO MUÑOZ','paroserom@umariana.edu.co','1085905979',NULL,NULL,NULL,1),
(567,'DEICY NATHALIA PANTOJA GOYES','deicpantoja@umariana.edu.co','193201047',NULL,NULL,NULL,1),
(568,'ANA ISABEL VALLEJO NARVAEZ','anaisavanar@gmail.com','52792643',NULL,NULL,NULL,1),
(569,'CARLOS CÓRDOBA','carcob47@gmail.com','6747440',NULL,NULL,NULL,1),
(570,'MAURICIO ANDRES MARTÍNEZ OLIVA','maurmartinez@umariana.edu.co','1085244038',NULL,NULL,NULL,1),
(571,'NATHALIA ORTEGA ROSERO','nathuluna@gmail.com','1004235929',NULL,NULL,NULL,1),
(572,'JUAN CARLOS CASTILLO BRAVO','juancarcastillo@umariana.edu.co','1007389672',NULL,NULL,NULL,1),
(573,'DANIELA SOFÍA CORAL ','danielacoral@umariana.edu.co','1010138101',NULL,NULL,NULL,1),
(574,'KEVIN SANTIAGO ORTIZ BOTINA','keviortiz@umariana.edu.co','1193469762',NULL,NULL,NULL,1),
(575,'MARÍA TERESA RENDÓN GUERRERO ','mtrendon5@gmail.com','1085294745',NULL,NULL,NULL,1),
(576,'CARMENZA JANNETH BENAVIDES MELO','benavidesmelo@gmail.com','27082133',NULL,NULL,NULL,1),
(577,'ALEXANDRA TARAPUES CALDERÓN','letarapues@umariana.edu','1233191980',NULL,NULL,NULL,1),
(578,'DIANA SOLARTE NARVÁEZ','diana.solarte@uniminuto.edu','1085245633',NULL,NULL,NULL,1),
(579,'ERIKA TATIANA LOPEZ ALBAN','subggestioncliente@hinfantil.org','30736283',NULL,NULL,NULL,1),
(580,'LINA MARIA LOPEZ BARRETO','eduhila@hinfantil.org','52494837',NULL,NULL,NULL,1),
(581,'ANGUIE CATERINE VILLOTA LOPEZ','anguvillota@umariana.edu.co','1144089935',NULL,NULL,NULL,1),
(582,'ADRIANA CECILIA OSPINA MONTOYA','aospinamon1@uniminuto.edu.co','36953004',NULL,NULL,NULL,1),
(583,'NINY JOHANNA ERASO ROSERO','nerasoroser@uniminuto.edu.co','1085260589',NULL,NULL,NULL,1),
(584,'CLAUDIA ANDREA GUERRERO MARTINEZ1','claudia.guerrero@uniminuto.edu','1085262976',NULL,NULL,NULL,1),
(585,'DIANA MILENA RODRIGUEZ PABÓN','diana.rodriguez.p@gmail.com','37086073',NULL,NULL,NULL,1),
(586,'MARÍA CAMILA JARAMILLO BRAVO','cami504@hotmail.com','1085265735',NULL,NULL,NULL,1),
(587,'SARA VALENTINA ESPAÑA CUENCA','saraesppa@gmail.com','1004234423',NULL,NULL,NULL,1),
(588,'MARÍA CAROLINA HUERTAS DELGADO','carohuertas19@gmail.com','1004233644',NULL,NULL,NULL,1),
(589,'BRIGITH DANIELA RUANO NARVÁEZ','brigitruano04@gmail.com','1004135501',NULL,NULL,NULL,1),
(590,'DANIELA SEGURA CUENCA','danielaseguracuenca@gmail.com','1004191443',NULL,NULL,NULL,1),
(591,'NATALIA BASTIDAS LEMOS','nataliabl749@gmail.com','1000859970',NULL,NULL,NULL,1),
(592,'DIANA PAOLA MONCAYO HERRERA','dianapmh@gmail.com','36756939',NULL,NULL,NULL,1),
(593,'SANDRA PATRICIA GOMEZ JOJOA','sandragomez_01@yahoo.com','37081007',NULL,NULL,NULL,1),
(594,'DIÓGENES HUMBERTO REVELO','fernandarevelo335@gmail.com','5255830',NULL,NULL,NULL,1),
(595,'MARIANGEL VEGA CEBALLOS','bweiwei271@gmail.com','1081052327',NULL,NULL,NULL,1),
(596,'LUISA MARIA OJEDA GOMEZ','luisaojeda328@gmail.com','1004255452',NULL,NULL,NULL,1),
(597,'ISABELLA OJEDA GOMEZ','isabellaoojeda@gmail.com','1004255453',NULL,NULL,NULL,1),
(598,'CIELITO MORENO ARTURO','cielis2003@gmail.com','1004233277',NULL,NULL,NULL,1),
(599,'MARCO ANTONIO CHAMORRO LUCERO','marcochamorro@umariana.edu.co','98702794',NULL,NULL,NULL,1),
(600,'MARÍA ALEJANDRA MARTÍNEZ ACOSTA','mariaalejandramartinezacosta@gmail.com','1004190457',NULL,NULL,NULL,1),
(601,'TATIANA MARGARITA CARRERA ROMERO','tatianacr025@gmail.com','1004508327',NULL,NULL,NULL,1),
(602,'PEDRO MAURICIO ROMERO ORTEGA','mau_riciowf@hotmail.com','1085315624',NULL,NULL,NULL,1),
(603,'MARIA JOSE CERÓN CADENA','mjceroncadena@gmail.com','1004235063',NULL,NULL,NULL,1),
(604,'MÓNICA CAMILA TORRES CÓRDOBA','c12032003@gmail.com','1004233850',NULL,NULL,NULL,1),
(605,'EDITH DE LOURDES HERNÁNDEZ NARVÁEZ','edith1.oct@gmail.com','27397037',NULL,NULL,NULL,1),
(606,'VÍCTOR MANUEL URBINA JARAMILLO','victorurbina13968@hotmail.com','1085305163',NULL,NULL,NULL,1),
(607,'JESUS AURELIO  TONGUINO DELGADO','dircientifica@hospitalsanrafaelpasto.com','12999246',NULL,NULL,NULL,1),
(608,'ALEJANDRA DEYANIRA DIAZ','alejhita210@gmail.com','1085933711',NULL,NULL,NULL,1),
(609,'OSCAR ANDRES ARIAS BURGOS','trabajosocialhsr@hotmail.com','1085245279',NULL,NULL,NULL,1),
(610,'OSCAR ARTURO MONTENEGRO CORAL','omontenegro@umariana.edu.co','5312024',NULL,NULL,NULL,1),
(611,'EDITH DE LOURDES HERNÁNDEZ NARVÁEZ','edith1.oct@gmail.com','27397037',NULL,NULL,NULL,1),
(612,'MARÍA CAMILA LÓPEZ ROMO','mariacl97@hotmail.com','1233188437',NULL,NULL,NULL,1),
(613,'ANGELA MARÍA RIASCOS GRANIZO','riaskos1234@gmail.com','1085344636',NULL,NULL,NULL,1),
(614,'INGRID JOHANA LARA BENAVIDES','ingridjohanalara@hotmail.com','1085307987',NULL,NULL,NULL,1),
(615,'LUIS ALBERTO MONTENEGRO MORA','lmontenegro@umariana.edu.co','1085277350',NULL,NULL,NULL,1),
(616,'BOLIVAR ARTURO DELGADO','barturo@umariana.edu.co','12978543',NULL,NULL,NULL,1),
(617,'JOSE ANTONIO MENZA VALLEJO','jmenza@umariana.edu.co','12965227',NULL,NULL,NULL,1),
(618,'LUIS FRANCISCO MELO ROSERO','lfmelo@umariana.edu.co','7695137',NULL,NULL,NULL,1),
(619,'JUAN PABLO ARCOS VILLOTA','jarcos@umariana.edu.co','87491024',NULL,NULL,NULL,1),
(620,'SILVIA BERENICE ROCÍO MONCAYO QUIÑONEZ','smoncayo@umariana.edu.co','30723622',NULL,NULL,NULL,1),
(621,'EYNER FABIÁN CHAMORRO GUERRERO','echamorro@umariana.edu.co','97470930',NULL,NULL,NULL,1),
(622,'HUGO L. REINA R.','hreinar@umariana.edu.co','13069603',NULL,NULL,NULL,1),
(623,'YANET DEL SOCORRO VALVERDE RIASCOS','yvalverde@umariana.edu.co','30734168',NULL,NULL,NULL,1),
(624,'MARIA FERNANDA ACOSTA ROMO','macosta@umariana.edu.co','27080742',NULL,NULL,NULL,1),
(625,'FABIO DAVID URBANO BUCHELI','urbanobf@yahoo.com','17060861',NULL,NULL,NULL,1),
(626,'MARÍA PATRICIA OBANDO ENRÍQUEZ','mobando@umariana.edu.co','30717283',NULL,NULL,NULL,1),
(627,'CAROL ELIZABETH ENRIQUEZ CORDOBA','carolenriquez555@gmail.com','36950310',NULL,NULL,NULL,1),
(628,'CHRISTIAN IVAN PALACIOS BENAVIDES','chrpalacios@umariana.edu.co','1233194124',NULL,NULL,NULL,1),
(629,'LEIDY DIANA PEÑA','dalyfran15@hotmail.com','37087873',NULL,NULL,NULL,1),
(630,'YINA MARCELA BASTIDAS LAGOSBASTIDAS LAGOS','gerenciasterilize@gmail.com','59177209',NULL,NULL,NULL,1),
(631,'ALVARO HUGO GOMEZ ROSERO','agomez@umariana.edu.co','98390009',NULL,NULL,NULL,1),
(632,'MARIA DEL SOCORRO BUCHELI CAMPIÑP','msbucheli@umariana.edu.co','30722704',NULL,NULL,NULL,1),
(633,'LEIDY MARCELA GÓMEZ MELO','lmgomez@umariana.edu.co','1086359590',NULL,NULL,NULL,1),
(634,'HUGO ALBERTO CAMAPAÑA MURIEL','hacampana@iucesmag.edu.co','12986194',NULL,NULL,NULL,1),
(635,'CARLOS GUAZMAYAN','cgubes@yahoo.es','12962518',NULL,NULL,NULL,1),
(636,'ANDRÉS ORLANDO OVIEDO CABRERA','andresoviedo8@hotmail.es','13074100',NULL,NULL,NULL,1),
(637,'FRANLY ROMO MELO','franly_romo@hotmail.com','27091146',NULL,NULL,NULL,1),
(638,'SANTIAGO OCAÑA BUCHELI','socanabucheli@gmail.com','1019022013',NULL,NULL,NULL,1),
(639,'ELSA ROSARIO DIAZ TERAN','elsarosariodiaz@gmail.com','30722047',NULL,NULL,NULL,1),
(640,'JUAN CARLOS CAICEDO PORTILLA ','juancafe98@gmail.com','0',NULL,NULL,NULL,1),
(641,'DANEYRA RÍOS HERNÁNDEZ','danehernandz@gmail.com','0',NULL,NULL,NULL,1),
(642,'EDGAR DUVAN NARVAEZ ANDRADE','andradeedgar318@gmail.com','0',NULL,NULL,NULL,1),
(643,'ERICK PIÑEROS','erick.pineros@aunar.edu.co','0',NULL,NULL,NULL,1),
(644,'ORLANDO EFRAIN ORTIZ MORAN','orlefra82@hotmail.com','0',NULL,NULL,NULL,1),
(645,'CATALINA MUÑOZ ARTURO','catalinam.arturo@gmail.com','1020771708',NULL,NULL,NULL,1),
(646,'LILIAN DAYANA CRUZ CRUZ','c.informatica@aunar.edu.co',NULL,NULL,'2019-06-17 14:23:16',NULL,1),
(647,'KELLY DAVID LOPEZ ','kellydavidlopez@gmail.com','52962374',NULL,NULL,NULL,1),
(648,'JONATHAN BOLAÑOS CORAL','jonathan.bolanos.coral@gmail.com','87066499',NULL,NULL,NULL,1),
(649,'INGRITD FANNY CHAVES  BRAVO','ifachavesbravo@gmail.com','0',NULL,NULL,NULL,1),
(650,'JESUS  ANDRES NARVAEZ CANIZALES','jesus.narvaezc@autonoma.edu.co','0',NULL,NULL,NULL,1),
(651,'MARIA CRISTINA RIVERA VALLEJO','nenfisr@hotmail.com','0',NULL,NULL,NULL,1),
(652,'JESUS ALBERTO BOLAÑOS GOMEZ','jesusalberto1968@yahoo.es','0',NULL,NULL,NULL,1),
(653,'JOSE ANIBAL TULCAN AGUIRRE','jose.atenea@hotmail.com','0',NULL,NULL,NULL,1),
(654,'MARIA INES PANTOJA VILLARREAL','mapantoja@umariana.edu.co','59814365',NULL,NULL,NULL,1),
(655,'SEBASTIAN ANDRES ARCOS GOMEZ','sebastian_chandesu@hotmail.com','0',NULL,NULL,NULL,1),
(656,'GIOVANNI ALBEIRO HERNÁNDEZ PANTOJA','gihernandez@umariana.edu.co','87716986',NULL,NULL,NULL,1),
(657,'NUBIA GONZÁLEZ MARTÍNEZ','ngonzalez@umariana.edu.co','30725186',NULL,NULL,NULL,1),
(658,'MARIA ALEJANDRA RUIZ GARCIA','malejaruiz@hotmail.com','1085316719',NULL,NULL,NULL,1),
(659,'RENÉ QUINTERO MONTES','rennixq@gmail.com','0',NULL,NULL,NULL,1),
(660,'DIEGO DE LA ROSA','marketingmixcol@yahoo.es','0',NULL,NULL,NULL,1),
(661,'EDUARDO ALEXANDER NARVAEZ TERAN','alexandernarvaezt@hotmail.com','1086754411',NULL,NULL,NULL,1),
(662,'PABLO NICOLÁS GUERRERO ORTEGA','neoguerrero18@gmail.com','13070107',NULL,NULL,NULL,1),
(663,'JOHANA ANDREA DELGADO APRAEZ','johanadelgado@umariana.edu.co','1004728200',NULL,NULL,NULL,1),
(664,'ÁLVARO HUGO GÓMEZ ROSERO','agomez@umariana.edu.co','0',NULL,NULL,NULL,1),
(665,'FRANCO HERNAN GUERREFO CAICEDO','frguerrero@umariana.edu.co','0',NULL,NULL,NULL,1),
(666,'PAOLA ANDREA VALLEJO CHAMORRO','ingles@aunar.edu.co','0',NULL,NULL,NULL,1),
(667,'SANDRA JANNETH NARVÁEZ GUERRERO','sjnarvaezg@yahoo.com.co','0',NULL,NULL,NULL,1),
(668,'ALEJANDRA LUCIA NARVÁEZ HERRERA','alejandra.narvaez@aunar.edu.co','36759433',NULL,NULL,NULL,1),
(669,'CÉSAR OSWALDO IBARRA','cesar.ibarra@unad.edu.co','12975664',NULL,NULL,NULL,1),
(670,'FRANCISCO EMIRO CABRERA HURTADO','franciscoemirocabrera@gmail.com','0',NULL,NULL,NULL,1),
(671,'ANA CONSTANZA ROJAS','anrojaslato@gmail.com','59 314 384',NULL,NULL,NULL,1),
(672,'ANA GABRIELA MONTILLA LASSO','gabriela18montilla@gmail.com','0',NULL,NULL,NULL,1),
(673,'LUIS ALBERTO MONTENEGRO MORA','lmontenegro@umariana.edu.co','1085277350',NULL,NULL,NULL,1),
(674,'ANDREA RENGIFO RENGIFO','andrearengiforengifo@yahoo.es','59831050',NULL,NULL,NULL,1),
(675,'FRANCO ANDRÉS MONTENEGRO CORAL','franco.montenegro@campusucc.edu.co','15814473',NULL,NULL,NULL,1),
(676,'MARGOTH TERESA GALLARDO CERÓN','mtgallardo@iucesmag.edu.co','27451052',NULL,NULL,NULL,1),
(677,'ANA MILENA ORTIZ CASANOVA','ana.ortiz@aunar.edu.co','37080967',NULL,NULL,NULL,1),
(678,'MAGALY MORA','betty.mora@aunar.edu.co','0',NULL,NULL,NULL,1),
(679,'EDISSON HERNANDO PAGUATIAN TUTISTAR','ingpaguatiant@gmail.com','5207567',NULL,NULL,NULL,1),
(680,'SANDRA INSUASTY CORDOBA ','sandra-insuasty@hotmail.com','59312965',NULL,NULL,NULL,1),
(681,'OSCAR EDUARDO CADENA OBANDO','oscar.bitacora@yahoo.es','13070576',NULL,NULL,NULL,1),
(682,'GEOVANNY ALEXANDER ABAUNZA','gabaunza@usal.es','1113626356',NULL,NULL,NULL,1),
(683,'CLAUDIA DEL PILAR MORA VANEGAS','claudia.mora@aunar.edu.co','1012331180',NULL,NULL,NULL,1),
(684,'EDITH ALEXANDRA LUNA ACOSTA','eluna@umariana.edu.co','59837276',NULL,NULL,NULL,1),
(685,'DIANA MARIA REVELO TOVAR','diana.revelo@aunar.edu.co','36752956',NULL,NULL,NULL,1),
(686,'HARRY ZAMBRANO ROSERO','harryza4@gmail.com','0',NULL,NULL,NULL,1),
(687,'ZORAYDA BENAVIDES NARVÁEZ','sorayamarth@gmail.com','0',NULL,NULL,NULL,1),
(688,'JIMMY CABRERA MEZA','jocm14@gmail.com','12999655',NULL,NULL,NULL,1),
(689,'IRINA JURADO PAZ','irina.jurado@aunar.edu.co','0',NULL,NULL,NULL,1),
(690,'ARTURO OBANDO IBARRA','arturobando@narino.gov.co','12955349',NULL,NULL,NULL,1),
(691,'JESÚS ANDRÉS NARVÁEZ CANIZALES','jesus.narvaezc@autonoma.edu.co','1085297938',NULL,NULL,NULL,1),
(692,'ÁLVARO DARÍO PABÓN ROJAS','alvaro.pabon.rojas@gmail.com','98399313',NULL,NULL,NULL,1),
(693,'GINA MELISSA MONTEZUMA OJEDA','ginamelimontezuma@gmail.com','1085327300',NULL,NULL,NULL,1),
(694,'JOSE OSCAR ZAMBRANO CANCHALA','oscarzambranoc@gmail.com','13070411',NULL,NULL,NULL,1),
(695,'CAMILO HERNANAN SOLARTE PATIÑO ','camsolarte@umariana.edu.co','1085343893',NULL,NULL,NULL,1),
(696,'ELSA ROSARIO DIAZ ','ediaz@umariana.edu.co','30722047',NULL,NULL,NULL,1),
(697,'JANIO CALDAS LUZEIRO ','jcaldas@iucesmag.edu.co','6567679',NULL,NULL,NULL,1),
(698,'MAURICIO ANDRES MARTINEZ OLIVA ','maumartinez@umariana.edu.co','1085244038',NULL,NULL,NULL,1),
(699,'LYDIA MIRANDA ','lmiranda@umariana.edu.co','29433083',NULL,NULL,NULL,1),
(700,'ANDRES MAURICIO BOTINA HERRERA ','andrbotina@umariana.edu.co ','123319037',NULL,NULL,NULL,1),
(701,'MUARICIO ALEJANDRO PALACIOS ERAZO ','mariopalacios@umariana.edu.co ','1017163169',NULL,NULL,NULL,1),
(702,'LUIS FELIPE ANDRADE BASTIDAS ','luisfandrade@umariana.edu.co','1085345509',NULL,NULL,NULL,1),
(703,'DAVID SEBASTIAN MARTINEZ ','davidmartinez@umariana.edu.co ','1085327450',NULL,NULL,NULL,1),
(704,'CAMILA PAZ MORENO ','mariacapaz@umariana.edu.co','1085317040',NULL,NULL,NULL,1),
(705,'MARIA RIVERA','nenfisr@hotmail.com','59812964',NULL,NULL,NULL,1),
(706,'DIANA PAOLA MONCAYO ','dianapmh@gmail.com','36756939',NULL,NULL,NULL,1),
(707,'OSCAR ALEJANDRO PANTOJA PANTOJA ','oscarpantoja@umariana.edu.co ','1004189851',NULL,NULL,NULL,1),
(708,'CARLOS ERNESTO DIAZ BASANTE ','carlosdiaz@umariana.edu.co','1082749257',NULL,NULL,NULL,1),
(709,'MATEO FABIAN ALVAREZ ERAZO ','matalvarez@umariana.edu.co','1084335964',NULL,NULL,NULL,1),
(710,'JAIME GAVILANES ','jimmigavilanes@','5173686',NULL,NULL,NULL,1),
(711,'MARIA DEL PILAR RINCON ','mapilarincond@hormail.com','41510564',NULL,NULL,NULL,1),
(712,'MARIA HELENA DIAZ ','mdiaz1501@gmail.com','30726043',NULL,NULL,NULL,1),
(713,'AYLEM YELA ROMO ','ayyela@umariana.edu.co','27302657',NULL,NULL,NULL,1),
(714,'DAVID ALEJANDRO SANTACRUZ CARVAJAL','davsantacruz@umariana.edu.co','1193273265',NULL,NULL,NULL,1),
(715,'CESAR RENDON ALVAREZ ','carendon@umariana.edu.co','1087192622',NULL,NULL,NULL,1),
(716,'CARMEN NARVAEZ ERAZO ','cnarvaez@umariana.edu.co','30744998',NULL,NULL,NULL,1),
(717,'GLORIA ARISTIZABAL ','g.aristi@live.com','24318049',NULL,NULL,NULL,1),
(718,'ELENA DIAZ ','ndiaz1501@gmail.com','30726043',NULL,NULL,NULL,1),
(719,'SANDRA PAZ ','sandralpaz@hotmail.com','59832148',NULL,NULL,NULL,1),
(720,'SANDRA ROCIO TORRES','sguerrroto@umariana.edu.co','59834935',NULL,NULL,NULL,1),
(721,'DEICY NATHALIA PANTOJA ','deicpantoja@umariana.edu.co','1193201047',NULL,NULL,NULL,1),
(722,'GUILLERMO ANDRES RIOS PANTOJA','guillorios@gmail.com','5204152','2019-06-13 20:07:10','2019-06-13 20:09:26',NULL,1),
(752,'DANNY MARTINEZ CEPEDA','hsey1111@gmail.com','13071798',NULL,NULL,NULL,3),
(753,'RAMON GONZALEZ','ramong1983@hotmail.com','87060881',NULL,NULL,NULL,3),
(754,'LAURA CORREAL ','laura_correal@upc.edu.co','1026596094',NULL,NULL,NULL,3),
(755,'OSWALDO PEREZ ','pauloperez67@gmail.com','12993073',NULL,NULL,NULL,3),
(756,'ESTHER MORA ','pauloperez67@gmail.com','27303823',NULL,NULL,NULL,3),
(757,'SARA MORA PANTOJA ','saristefmp218@gmail.com','1004550276',NULL,NULL,NULL,3),
(758,'EDGAR ALEXANDER PINZA ','avveit1397@hotmail.com','12747377',NULL,NULL,NULL,3),
(759,'ANGELA XIMENA GUERRA','angelitaguerra0_0@hotmail.com','1085307020',NULL,NULL,NULL,3),
(760,'DANIEL HENAO','angelitaguerra0_0@hotmail.com','8164938',NULL,NULL,NULL,3),
(761,'MARCELA CHAMORRO','marpolula@gmail.com','1085270262',NULL,NULL,NULL,3),
(762,'ESTEBAN PANTOJA ','escapadelo@hotmail.com','1085283214',NULL,NULL,NULL,3),
(763,'MANUELA LUCERO SERRANO ','maneluce96@gmail.com','1085329499',NULL,NULL,NULL,3),
(764,'SERGIO VILLOTA ','samexvl@hotmail.com','87061424',NULL,NULL,NULL,3),
(765,'ASDRUBAL URBINA ','artaios69@gmail.com','12996703',NULL,NULL,NULL,3),
(766,'ANA SALAZAR','nativamestiza@gmail.com','52708275',NULL,NULL,NULL,3),
(767,'DAVID STEVAN OCAÑA ','dsor.22@gmail.com','1085320485',NULL,NULL,NULL,3),
(768,'ANDREA CABRERA ','pao_andrea1606@hotmail.com','27091268',NULL,NULL,NULL,3),
(769,'ADRIANA MUÑOZ ','pao_andrea1606@hotmail.com','30735315',NULL,NULL,NULL,3),
(770,'JACQUELINE BASTIDAS ','jacquebastidas@hotmail.com','36754782',NULL,NULL,NULL,3),
(771,'MARCELA TULCAN ','marcelatulcan1991@gmail.com','1085291399',NULL,NULL,NULL,3),
(772,'XIMENA RODRIGUEZ ','arq.ximenarodriguez@gmail.com','1085256489',NULL,NULL,NULL,3),
(773,'VALENTINA BOCANEGRA ','macuahua@gmail.com','41959140',NULL,NULL,NULL,3),
(774,'MERCEDES VILLOTA ','ixmechuda@gmail.com','57083460',NULL,NULL,NULL,3),
(775,'MARCO GOMEZ ','marco,gomez.mk@gmail.com','5278466',NULL,NULL,NULL,3),
(776,'LILIANA CHAVES ','cilene_libiana@hotmail.com','36751751',NULL,NULL,NULL,3),
(777,'TANIA ANGELICA ERAZO','erazotaniaz@gmail.com','1081273289',NULL,NULL,NULL,3),
(778,'DARIO VARGAS ','gedavaca@gmail.com','1085279561',NULL,NULL,NULL,3),
(779,'YAMILE ORTIZ CANCHALA ','yamile.ortiz@gmail.com','1085248694',NULL,NULL,NULL,3),
(780,'KARLA JESSENIA KREISBERGER ','kakreisberger@gmail.com','1085248694',NULL,NULL,NULL,3),
(781,'PAMELA LASSO','cuandoelgenerosuena@gmail.com','37082412',NULL,NULL,NULL,3),
(782,'ANA ERAZO','mensajera14@hotmail.com','1004730805',NULL,NULL,NULL,3),
(783,'ERIKA BURBANO ','erikabur_135@hotmail.com ','25292503',NULL,NULL,NULL,3),
(784,'CLAUDIA ERASO ','cl2091@gmail.com ','598232110',NULL,NULL,NULL,3),
(785,'ANA PATRICIA ENRIQUEZ ','anaenriquez121@gmail.com','1085277660',NULL,NULL,NULL,3),
(786,'CATHERIN DURAN ','irisadacora@gmail.com','1030547653',NULL,NULL,NULL,3),
(787,'JOSE ANDRES FUENTES ','jose.andres.fuentes@gmail.com',NULL,NULL,NULL,NULL,3),
(788,'MIRIAM LINCE ','milicl55@hotmail.com','30712594',NULL,NULL,NULL,3),
(789,'JULIO DORADO DAVILA','doradojota@gmali,com','12986594',NULL,NULL,NULL,3),
(790,'ALBA YENNY NUPAN ','ajnupanc@unal.edu.co','101722612',NULL,NULL,NULL,3),
(791,'JANNETH ALEJANDRA MARTINEZ PEDREROS','amartinezpedreros@gmail.com','52963806',NULL,NULL,NULL,3),
(792,'LAURA WAGNER ARENAS','ldwargnera@gmail.com','1006291550',NULL,NULL,NULL,3),
(793,'MARIA ROSERO','mariaxrosero@gmail.com','37086238',NULL,NULL,NULL,3),
(794,'ELIENNE BOISGUERIN','elienne@habitatvivo.com',NULL,NULL,NULL,NULL,3),
(795,'CAMILO JOSUE BLANCO ','camilo@habitatvivo.com','1121902672',NULL,NULL,NULL,3),
(796,'GLADYS AGREDA ','glamontenegro@gmail.com','30728626',NULL,NULL,NULL,3),
(797,'LINA TATIANA RUBIO','linatatianag.rubiom@gmail.com','53107218',NULL,NULL,NULL,3),
(798,'GIOVANNI CASTEBLANCO ','giokastel@gmail.com','79890571',NULL,NULL,NULL,3),
(799,'ADRIANA DEL CARMEN ERASO ','adrianitaeraso@gmail.com ','59825435',NULL,NULL,NULL,3),
(800,'EMILSEN ELIANA DE LA CRUZ ','emieliana2@gmail.com','27470430',NULL,NULL,NULL,3),
(801,'CARMEN AMPARO ARCOS ','amparoar_26@yahoo.es','30728691',NULL,NULL,NULL,3),
(802,'MONICA GARCES ','arq.moniarcos@gmail.com','59314936',NULL,NULL,NULL,3),
(803,'JULIANA REINA AYALA ','juli.rsl25@gmail.com','1004216692',NULL,NULL,NULL,3),
(804,'ITAC GIOSANA COLIVA ','iittaace@gmail.com','1085290490',NULL,NULL,NULL,3),
(805,'KAREN DANIELA PEREZ','daniela08.dk@gmail.com','1233189652',NULL,NULL,NULL,3),
(806,'EDISON GUANCHA ','edizonguancha@hotmail.com','98346408',NULL,NULL,NULL,3),
(807,'LUISA FERNANDA LOPEZ ','luisa.flq@gmail.com','1017254142',NULL,NULL,NULL,3),
(808,'MARLY ZAMBRANO PINEDA ','marzambrano77@gmail.com','37084476',NULL,NULL,NULL,3),
(809,'CHRISTHOPER PEES ','tobaltof@hotmail.com','1128739',NULL,NULL,NULL,3),
(810,'OMAR SANCHEZ ','logomar88@gmail.com','1030550186',NULL,NULL,NULL,3),
(811,'MARIA ALEJANDRA ARIAS ','maria.alejandra.arias.b@gmail.com','59314640',NULL,NULL,NULL,3),
(812,'DORIS  CHECA CORAL ','doche68@hotmail.com','30738361',NULL,NULL,NULL,3),
(813,'JAIRO JURADO','jairoajurado@gmail.com','12749572',NULL,NULL,NULL,3),
(814,'LAURA PEREZ ','mlauraperezzp@gmail.com','28118878',NULL,NULL,NULL,3),
(815,'MAURICIO BASTIDAS ','mauriciobastidasb@gmail.com','87713444',NULL,NULL,NULL,3),
(816,'ALEJANDRA REINA LOPEZ ','alejitawb@hotmail.com','1087673764',NULL,NULL,NULL,3),
(817,'DIANA MARIA ORTIZ ','dianaortiz712@gmail.com','1085269206',NULL,NULL,NULL,3),
(818,'DIANA GUERRERO PEREZ ','dmguerrerop@misena.edu.co ','37084465',NULL,NULL,NULL,3),
(819,'CARLOS GRANDA ','rastararycolombia@hotmail.com','98392136',NULL,NULL,NULL,3),
(820,'DEIBBIE VERGARA ','deibby-95@hotmail.com','1214731893',NULL,NULL,NULL,3),
(821,'ANNA CAMPS ','anna_camps_pwgdemont@hotmail.com',NULL,NULL,NULL,NULL,3),
(822,'MELANNY HERNANDEZ ','melannyh@gmail.com',NULL,NULL,NULL,NULL,3),
(823,'LUIS HIDROBO ','luisdavid10899@gmail.com','1121506900',NULL,NULL,NULL,3),
(824,'VALENTINA AGUIRRE ','maupaguirre@hotmail.com','1085328993',NULL,NULL,NULL,3),
(825,'JOSE QUIROGA ','josequirogauribea@gmail.com',NULL,NULL,NULL,NULL,3),
(826,'SEBASTIAN CALPA TELLO ','secalpa@hotmail.com','1085262342',NULL,NULL,NULL,3),
(827,'HECTOR HERNANDO NARVAEZ ','nato77@misena.edu.co','98398495',NULL,NULL,NULL,3),
(828,'EMIRO MARMAN GUZMAN ','emiromg@gmail.com','12951291',NULL,NULL,NULL,3),
(829,'LUCIA GALENO','alunasg@yahoo.com','31921430',NULL,NULL,NULL,3),
(830,'ANDRES GAITAN ','andres84artes@gmail.com','93300175',NULL,NULL,NULL,3),
(831,'FRANCO VELASQUEZ ','franco1velasquez@yahoo.com','12990058',NULL,NULL,NULL,3),
(832,'NATHALIA MUÑOZ ','amelicinga@hotmail.com','36758666',NULL,NULL,NULL,3),
(833,'ARTURO MUÑOZ ','urdimbream@hotmail.com','12958563',NULL,NULL,NULL,3),
(834,'ALEYDA QUINCHUA','aquinchuaerazo@gmail.com','27190138',NULL,NULL,NULL,3),
(835,'MARIA VERGARA ','leocadia17@hotmail.com','31096897',NULL,NULL,NULL,3),
(836,'PAULA GUERRERO ASCUNTAR ','killari7@hotmail.com','27090577',NULL,NULL,NULL,3),
(837,'JUAN ESCOBAR ','jescobuili53@gmail.com','1126004520',NULL,NULL,NULL,3),
(838,'NICOLAS SANCHEZ ','nicolas_sanchez@javeriana.edu.co','1019123123',NULL,NULL,NULL,3),
(839,'MESIAS MUÑOZ ','mesiasm19@hotmail.com','84027213',NULL,NULL,NULL,3),
(840,'MARIA CAMILA MORENO','maria_camila981996@hotmail.com','1152459615',NULL,NULL,NULL,3),
(841,'ADRIANA MARIA HERRAN ','mariaherrang@gmail.com','1022435251',NULL,NULL,NULL,3),
(842,'NASARE SANTA CRUZ ','nasare3043@hotmail.com','301734478',NULL,NULL,NULL,3),
(843,'OMAR SALVADOR CHICAZA ','ioratuh0987@gmail.com','87550653',NULL,NULL,NULL,3),
(844,'JHONN NARVAEZ ','alasdeestrella@gmail.com','16931365',NULL,NULL,NULL,3),
(845,'VALENTINA AGUIRRE ','maveaguirre@hotmail.com','1085328993',NULL,NULL,NULL,3),
(846,'CAMILO MORA','camilomora100@hotmail.com','94452616',NULL,NULL,NULL,3),
(847,'MARIA HELENA MUÑOZ ','munozpaysumaria@gmail.com','30841880',NULL,NULL,NULL,3),
(848,'AURA MARINA BASTIDAS ','amaba20@hotmail.com','30712609',NULL,NULL,NULL,3),
(849,'ALVARO CERVELION','alcelo12@hotmail.com','12960055',NULL,NULL,NULL,3),
(850,'WILLIAM DAVID IBARRA ','wdibarram@unicauca.edu.co ','1085907944',NULL,NULL,NULL,3),
(851,'JESUS JAVIER MINALAN ','jjesusmit@gmail.com','87514905',NULL,NULL,NULL,3),
(852,'EDITH ELIZABETH CORDOBA ','cordobae3009@gmail.com','1082749241',NULL,NULL,NULL,3),
(853,'JHON STIVEN GUANGA MERA ','jhonstid1@gmail.com','1089294936',NULL,NULL,NULL,3),
(854,'XATLI CAMILA MOYA ','xatica86@hotmail.com','1070005114',NULL,NULL,NULL,3),
(855,'LILIANA ALPALA ','liriosnandinos@gmail.com','1088591016',NULL,NULL,NULL,3),
(856,'LILIAN AMALITH MIMALCHI','doradojota@gmali.com',NULL,NULL,NULL,NULL,3),
(857,'SEBASTIAN PALACIOS OJEDA ','palacios.sbs@gmail.com','1085264516',NULL,NULL,NULL,3),
(858,'TATIANA CORAL MEDINA ','tatiana.9723@gmail.com ','1085338257',NULL,NULL,NULL,3),
(859,'MIGUEL TORSKE ','yaleaninapeima@gmail.com','201769320',NULL,NULL,NULL,3),
(860,'CARLOS ROJAS ','carlos@habitatvivo.com','79754192',NULL,NULL,NULL,3),
(861,'JANNETH ARAUJO','araujor.janeth@gmail.com','1130594591',NULL,NULL,NULL,3),
(862,'ELIANA GUERRA MARTINEZ ','elianaguerra199@gmail.com','1233192861',NULL,NULL,NULL,3),
(863,'DANTE MORADO','yayitomgames@gmail.com ','1996875',NULL,NULL,NULL,3),
(864,'AURA ISABEL SILVA ','aurasilva@hotmail.com','1118564188',NULL,NULL,NULL,3),
(865,'LUKAS VARGAS ','lukasvargas@hotmail.com','71818835',NULL,NULL,NULL,3),
(866,'DANIEL ARDITA ','daniardita@gmail.com',NULL,NULL,NULL,NULL,3),
(867,'LAURA VICTORIA MAMIAN ','lauravml@gmail.com','34317050',NULL,NULL,NULL,3),
(868,'JAIRO ESPAÑA ','jaiveup@gmail.com','5207578',NULL,NULL,NULL,3),
(869,'PAULA ROMO ','proyectode5101@gmail.com ','36953978',NULL,NULL,NULL,3),
(870,'CRISTIAN PAZ ','ara.cristhianpaz@gmail.com','1085285992',NULL,NULL,NULL,3),
(871,'CAMILO FIQUE MORALES ','rom.f.cam@gmail.com','1072668730',NULL,NULL,NULL,3),
(872,'FREDDY MAURICIO LOPEZ ','alcaravam@gmail.com','80765668',NULL,NULL,NULL,3),
(873,'CRISTIAN JIMENEZ ','cristianjimenezilustrador@gmail.com','80891541',NULL,NULL,NULL,3),
(874,'ANA LUCIA ZAMBRANO ','analuzambranopantoja@gmail.com','27109153',NULL,NULL,NULL,3),
(875,'SOFIA CABRERA ','sophiekabrera@gmail.com','1085281104',NULL,NULL,NULL,3),
(876,'SUSANA ANDREA LOPEZ ','sl981217@gmail.com','1085344097',NULL,NULL,NULL,3),
(877,'MARIA JOSE MELO DELGADO ','mariajmelod@hotmail.com','1018483541',NULL,NULL,NULL,3),
(878,'DIANA JAZMIN SANCHEZ ','dianaudenar@gmail.com','1085316810',NULL,NULL,NULL,3),
(879,'LORENA VIEIRA DIAZ ','lorenavieira8@gmail.com','1010240374',NULL,NULL,NULL,3),
(880,'OSWALDO GOMEZ VARGAS ','osgomezvargas@gmail.com ','98215252',NULL,NULL,NULL,3),
(881,'LEIDY ROCIO GUERRERO ','lrguerreroo@unal.edu.co','1033734532',NULL,NULL,NULL,3),
(882,'PAULA MOGOLLON GARCIA ','pamogollong@unal.edu.co','1085299361',NULL,NULL,NULL,3),
(883,'ANGELA MALTE ','angelamaltete@gmail.com','1085270065',NULL,NULL,NULL,3),
(884,'TINALYD BOLAÑOS GALLARDO ','tynalyd.bol@gmail.com','1045016432',NULL,NULL,NULL,3),
(885,'BEATRIZ HELENA CHACÓN ','bettychani861@hotmail.com','43668591',NULL,NULL,NULL,3),
(886,'CARLOS JOSE VILLATE ','carlos-villate96@hotmail.com','1020816472',NULL,NULL,NULL,3),
(887,'ESPERANZA AGREDA ','tania029@yahoo.com','30711375',NULL,NULL,NULL,3),
(888,'MARTIZA SANCHEZ ','coloresmari@gmail.com ','32243119',NULL,NULL,NULL,3),
(889,'CARMEN MARINA LUNA MORA ','carmenlunamar@gmail.com','59650655',NULL,NULL,NULL,4),
(890,'ANDREA DE JESUS RODRIGUEZ ','andready17@hotmail.com','1086923653',NULL,NULL,NULL,4),
(891,'TATIANA DELGADO GARZON ','tatianadelagogarzon@gmail.com','37087277',NULL,NULL,NULL,4),
(892,'PIEDAD VALLEJO ','piedad030@gmail.com','30722642',NULL,NULL,NULL,4),
(893,'VICTOR EFRAIN MUÑOZ ','victorefrain.munoz@gmail.com','12959919',NULL,NULL,NULL,4),
(894,'LINA MARIA MELO JARAMILLO ','lina-maria97@hotmail.com','1018998769',NULL,NULL,NULL,4),
(895,'MYRIAM MONCAYO ','moncayob7@gmail.com','27433959',NULL,NULL,NULL,4),
(896,'PIEDAD ESTRADA ANGEL ','elena_estrada_@hotmail.com ','22024527',NULL,NULL,NULL,4),
(897,'MIRIAM JANETH PORTILLO PORTILLO ','mirianportillo@hotmail.com','27548720',NULL,NULL,NULL,4),
(898,'GUSTAVO PEREZ LOPEZ ','tavopasto2@gmail.com','12993320',NULL,NULL,NULL,4),
(899,'FREDY ALEJANDRO DIAZ ','fdiaz@sednarino.gov.co','98215083',NULL,NULL,NULL,4),
(900,'MARIA ELSA JURADO GRIJALBA ','elsajurado@narino.gov.co','36850215',NULL,NULL,NULL,4),
(901,'LUIS ULPIANO TATAMUES ','ltatamuesgarcia@gmail.com','12755377',NULL,NULL,NULL,4),
(902,'OMAR EDGARDO MUÑOZ ','oemunoz21@hotmail.com','98345785',NULL,NULL,NULL,4),
(903,'NIDIA CRISTINA MUÑOZ FIGUEROA ','nicrimufi@hotmail.com','1030564624',NULL,NULL,NULL,4),
(904,'GINA VILLOTA ','ginavillota@narino.gov.co','59829119',NULL,NULL,NULL,4),
(905,'ADRIANA ACHICANOY ','adrianaachicanoy@hotmail.com','59818664',NULL,NULL,NULL,4),
(906,'DAISY UMAÑA TRIVIÑO ','didasge@hotmail.com','1085246548',NULL,NULL,NULL,4),
(907,'ERNESTINA BASTIDAS ','didasge@hotmail.com','30722427',NULL,NULL,NULL,4),
(908,'ANDREA ROMO ','arc1028@hotmail.com','1085292563',NULL,NULL,NULL,4),
(909,'AURA SOLANO ','aurasol.34@gmail.com','30735087',NULL,NULL,NULL,4),
(910,'JOHANNA DELGADO APRAEZ ','johanapraez14@gmail.com','1004728200',NULL,NULL,NULL,4),
(911,'TATIANA BURBANO ','burbano.tatiana@correounivalle.edu.co','10877522331',NULL,NULL,NULL,4),
(912,'ANA ISABEL FAJARDO RAMIREZ ','chavita12@hotmail.com','1017146133',NULL,NULL,NULL,4),
(913,'GINNA ALEJANDRA VELASQUEZ ','alejandravelasquez5@hotmail.com','37084718',NULL,NULL,NULL,4),
(914,'DORIS FABIOLA ORTIZ ','ortizdorisfabiola@gmail.com','27097389',NULL,NULL,NULL,4),
(915,'EMERITA RODRIGUEZ DAZA ','eroda11@hotmail.com','30724513',NULL,NULL,NULL,4),
(916,'LUIS IGNACIO DELGADO ','gestionorganizacional@narino.gov.co','98326613',NULL,NULL,NULL,4),
(917,'MAGDA HIDALGO ','magda.hidalgo21@hotmail.com','59312191',NULL,NULL,NULL,4),
(918,'GERMAN RIGOBERTO JURADO PANTOJA ','rigobertojurado@narino.gov.co','5306085',NULL,NULL,NULL,4),
(919,'MARIA DEL SOCORRO CUS','maria.cus0975@gmail.com','27401358',NULL,NULL,NULL,4),
(920,'MARIO DELGADO ARTURO','delamar11@hotmail.com','12962882',NULL,NULL,NULL,4),
(921,'Lizeth Obando',NULL,'1085248222','2019-11-29 18:40:37','2019-11-29 18:40:37',NULL,3),
(922,'Jesus Andres Narváez',NULL,'1085 297 938','2019-11-29 18:41:05','2019-11-29 18:41:05',NULL,3),
(923,'Angela Sorany Caicedo Romo',NULL,'1086135632','2019-11-29 18:41:31','2019-11-29 18:41:31',NULL,1),
(924,'Angela Sorany Caicedo Romo',NULL,'1086135632','2019-11-29 18:42:06','2019-11-29 18:42:06',NULL,3),
(925,'Xiomara Acevedo Navarro',NULL,'1140841479','2019-12-02 22:37:48','2019-12-02 22:37:48',NULL,3),
(926,'Xiomara Acevedo Navarro',NULL,'1140841479','2019-12-02 22:37:51','2019-12-02 22:37:51',NULL,3),
(927,'Xiomara Acevedo Navarro',NULL,'1140841479','2019-12-02 22:38:17','2019-12-02 22:38:17',NULL,4),
(928,'Esteban Marín Quintero',NULL,'1061435173','2019-12-02 22:39:03','2019-12-02 22:39:03',NULL,3),
(929,'Esteban Marín Quintero',NULL,'1061435173','2019-12-02 22:39:06','2019-12-02 22:39:06',NULL,3),
(930,'Esteban Marin Quintero',NULL,'1061435173','2019-12-02 22:39:23','2019-12-02 22:39:23',NULL,4),
(931,'Andra Estefania Morales Mora',NULL,'1085303668','2019-12-03 19:03:57','2019-12-03 19:03:57',NULL,3),
(932,'Helena Sofia Morales Mora',NULL,'10042013907','2019-12-03 19:04:48','2019-12-03 19:04:48',NULL,3),
(933,'Helena Sofia Morales Mora',NULL,'10042013907','2019-12-03 19:04:50','2019-12-03 19:04:50',NULL,3),
(934,'Helena Sofia Morales Mora',NULL,'10042013907','2019-12-03 19:04:52','2019-12-03 19:04:52',NULL,3),
(935,'Cristian Camilo Jose Guzman',NULL,'80851541','2019-12-12 22:52:33','2019-12-12 22:52:33',NULL,3),
(936,'Cristian Camilo Jose Guzman',NULL,'80851541','2019-12-12 22:52:36','2019-12-12 22:52:36',NULL,3),
(937,'Cristian Camilo Jose Guzman',NULL,'80851541','2019-12-12 22:52:38','2019-12-12 22:52:38',NULL,3),
(938,'Gina Paola León Alarcón',NULL,'1032469187','2019-12-12 22:53:30','2019-12-12 22:53:30',NULL,3),
(939,'Daniela Rodriguez Carreño',NULL,'1070970977','2019-12-12 22:57:27','2019-12-12 22:57:27',NULL,3),
(940,'Diego Francisco Barrera Imbajoa',NULL,'1085278397','2019-12-12 23:03:38','2019-12-12 23:03:38',NULL,3);

/*Table structure for table `data_rows` */

DROP TABLE IF EXISTS `data_rows`;

CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `data_rows` */

insert  into `data_rows`(`id`,`data_type_id`,`field`,`type`,`display_name`,`required`,`browse`,`read`,`edit`,`add`,`delete`,`details`,`order`) values 
(1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),
(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),
(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),
(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),
(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),
(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),
(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),
(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),
(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),
(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),
(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),
(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),
(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),
(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),
(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),
(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),
(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),
(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),
(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),
(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),
(21,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9),
(22,4,'id','number','ID',1,0,0,0,0,0,NULL,1),
(23,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),
(24,4,'order','text','Order',1,1,1,1,1,1,'{\"default\":1}',3),
(25,4,'name','text','Name',1,1,1,1,1,1,NULL,4),
(26,4,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),
(27,4,'created_at','timestamp','Created At',0,0,1,0,0,0,NULL,6),
(28,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),
(29,5,'id','number','ID',1,0,0,0,0,0,NULL,1),
(30,5,'author_id','text','Author',1,0,1,1,0,1,NULL,2),
(31,5,'category_id','text','Category',1,0,1,1,1,0,NULL,3),
(32,5,'title','text','Title',1,1,1,1,1,1,NULL,4),
(33,5,'excerpt','text_area','Excerpt',1,0,1,1,1,1,NULL,5),
(34,5,'body','rich_text_box','Body',1,0,1,1,1,1,NULL,6),
(35,5,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),
(36,5,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}',8),
(37,5,'meta_description','text_area','Meta Description',1,0,1,1,1,1,NULL,9),
(38,5,'meta_keywords','text_area','Meta Keywords',1,0,1,1,1,1,NULL,10),
(39,5,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),
(40,5,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,12),
(41,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,13),
(42,5,'seo_title','text','SEO Title',0,1,1,1,1,1,NULL,14),
(43,5,'featured','checkbox','Featured',1,1,1,1,1,1,NULL,15),
(44,6,'id','number','ID',1,0,0,0,0,0,NULL,1),
(45,6,'author_id','text','Author',1,0,0,0,0,0,NULL,2),
(46,6,'title','text','Title',1,1,1,1,1,1,NULL,3),
(47,6,'excerpt','text_area','Excerpt',1,0,1,1,1,1,NULL,4),
(48,6,'body','rich_text_box','Body',1,0,1,1,1,1,NULL,5),
(49,6,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}',6),
(50,6,'meta_description','text','Meta Description',1,0,1,1,1,1,NULL,7),
(51,6,'meta_keywords','text','Meta Keywords',1,0,1,1,1,1,NULL,8),
(52,6,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),
(53,6,'created_at','timestamp','Created At',1,1,1,0,0,0,NULL,10),
(54,6,'updated_at','timestamp','Updated At',1,0,0,0,0,0,NULL,11),
(55,6,'image','image','Page Image',0,1,1,1,1,1,NULL,12),
(176,16,'id','text','Id',1,0,0,0,0,0,'{}',1),
(177,16,'name','text','Nombre',1,1,1,1,1,1,'{}',2),
(178,16,'email','text','Email',0,1,1,1,1,1,'{}',3),
(179,16,'identified','text','Identificación',0,1,1,1,1,1,'{}',4),
(180,16,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',5),
(181,16,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',6),
(182,16,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,'{}',7),
(183,17,'id','text','Id',1,0,0,0,0,0,'{}',1),
(184,17,'name','text','Nombre',1,1,1,1,1,1,'{}',2),
(185,17,'observation','text','Observación',0,1,1,1,1,1,'{}',3),
(187,16,'certificate_hasone_topic_relationship','relationship','Tema',0,1,1,1,1,1,'{\"model\":\"App\\\\Topic\",\"table\":\"topics\",\"type\":\"belongsTo\",\"column\":\"topic_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',8),
(188,16,'topic_id','select_dropdown','topics',0,0,0,1,1,1,'{}',8),
(189,17,'created_at','timestamp','Creado',0,1,1,1,0,1,'{}',4),
(190,17,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),
(191,17,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,'{}',6),
(192,17,'image','image','Imagen',1,1,1,1,1,1,'{}',7),
(193,17,'name_padding_top','number','Nombre : Padding Superior',0,0,0,1,1,1,'{}',8),
(194,17,'name_padding_left','number','Nombre : Padding Izquierdo',0,0,0,1,1,1,'{}',9),
(195,17,'identified_padding_top','number','Identificación : Padding Superior',0,0,0,1,1,1,'{}',10),
(196,17,'identiffied_padding_left','number','Identificación : Padding Superior Izquierdo',0,0,0,1,1,1,'{}',11),
(198,17,'orientation','radio_btn','Orientación',0,1,1,1,1,1,'{\"default\":\"HORIZONTAL\",\"options\":{\"HORIZONTAL\":\"HORIZONTAL\",\"VERTICAL\":\"VERTICAL\"}}',12);

/*Table structure for table `data_types` */

DROP TABLE IF EXISTS `data_types`;

CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `data_types` */

insert  into `data_types`(`id`,`name`,`slug`,`display_name_singular`,`display_name_plural`,`icon`,`model_name`,`policy_name`,`controller`,`description`,`generate_permissions`,`server_side`,`details`,`created_at`,`updated_at`) values 
(1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2019-05-16 20:41:59','2019-05-16 20:41:59'),
(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2019-05-16 20:41:59','2019-05-16 20:41:59'),
(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2019-05-16 20:41:59','2019-05-16 20:41:59'),
(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,NULL,'2019-05-16 20:47:13','2019-05-16 20:47:13'),
(5,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,NULL,'2019-05-16 20:47:15','2019-05-16 20:47:15'),
(6,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,NULL,'2019-05-16 20:47:17','2019-05-16 20:47:17'),
(16,'certificates','certificates','Certificate','Certificates','voyager-certificate','App\\Certificate',NULL,NULL,NULL,1,0,'{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"asc\",\"default_search_key\":\"name\",\"scope\":null}','2019-06-11 17:44:43','2019-06-13 20:09:09'),
(17,'topics','topics','Topic','Topics','voyager-paper-plane','App\\Topic',NULL,NULL,NULL,1,0,'{\"order_column\":\"id\",\"order_display_column\":\"id\",\"order_direction\":\"asc\",\"default_search_key\":\"name\",\"scope\":null}','2019-06-11 22:09:57','2019-06-15 01:41:29');

/*Table structure for table `menu_items` */

DROP TABLE IF EXISTS `menu_items`;

CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `menu_items` */

insert  into `menu_items`(`id`,`menu_id`,`title`,`url`,`target`,`icon_class`,`color`,`parent_id`,`order`,`created_at`,`updated_at`,`route`,`parameters`) values 
(1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2019-05-16 20:42:01','2019-05-16 20:42:01','voyager.dashboard',NULL),
(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2019-05-16 20:42:01','2019-06-11 17:46:26','voyager.media.index',NULL),
(3,1,'Usuarios','','_self','voyager-person','#000000',NULL,4,'2019-05-16 20:42:01','2019-12-12 01:25:32','voyager.users.index','null'),
(4,1,'Roles','','_self','voyager-lock',NULL,NULL,3,'2019-05-16 20:42:01','2019-06-11 17:46:16','voyager.roles.index',NULL),
(5,1,'Herramientas','','_self','voyager-tools','#000000',NULL,9,'2019-05-16 20:42:02','2019-12-12 01:26:28',NULL,''),
(6,1,'Constructor de Menús','','_self','voyager-list','#000000',5,1,'2019-05-16 20:42:02','2019-12-12 01:26:43','voyager.menus.index','null'),
(7,1,'Bases de Datos','','_self','voyager-data','#000000',5,2,'2019-05-16 20:42:02','2019-12-12 01:27:21','voyager.database.index','null'),
(8,1,'Compass','','_self','voyager-compass',NULL,5,3,'2019-05-16 20:42:02','2019-05-16 21:22:31','voyager.compass.index',NULL),
(9,1,'BREAD','','_self','voyager-bread',NULL,5,4,'2019-05-16 20:42:02','2019-05-16 21:22:31','voyager.bread.index',NULL),
(10,1,'Configuraciones','','_self','voyager-settings','#000000',NULL,10,'2019-05-16 20:42:02','2019-12-12 01:27:10','voyager.settings.index','null'),
(11,1,'Hooks','','_self','voyager-hook',NULL,5,5,'2019-05-16 20:42:07','2019-05-16 21:22:31','voyager.hooks',NULL),
(12,1,'Categorías','','_self','voyager-categories','#000000',NULL,8,'2019-05-16 20:47:14','2019-12-12 01:26:13','voyager.categories.index','null'),
(13,1,'Publicaciones','','_self','voyager-news','#000000',NULL,6,'2019-05-16 20:47:17','2019-12-12 01:25:45','voyager.posts.index','null'),
(14,1,'Páginas','','_self','voyager-file-text','#000000',NULL,7,'2019-05-16 20:47:18','2019-12-12 01:25:56','voyager.pages.index','null'),
(21,1,'Certificados','','_self','voyager-certificate','#000000',22,1,'2019-06-11 17:44:43','2019-06-11 18:17:48','voyager.certificates.index','null'),
(22,1,'Registros','#','_self','voyager-pen','#000000',NULL,2,'2019-06-11 17:46:03','2019-06-11 17:46:16',NULL,''),
(23,1,'Temas','','_self','voyager-paper-plane','#000000',22,2,'2019-06-11 22:09:58','2019-06-12 20:35:26','voyager.topics.index','null');

/*Table structure for table `menus` */

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `menus` */

insert  into `menus`(`id`,`name`,`created_at`,`updated_at`) values 
(1,'admin','2019-05-16 20:42:01','2019-05-16 20:42:01');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2016_01_01_000000_add_voyager_user_fields',1),
(4,'2016_01_01_000000_create_data_types_table',1),
(5,'2016_05_19_173453_create_menu_table',1),
(6,'2016_10_21_190000_create_roles_table',1),
(7,'2016_10_21_190000_create_settings_table',1),
(8,'2016_11_30_135954_create_permission_table',1),
(9,'2016_11_30_141208_create_permission_role_table',1),
(10,'2016_12_26_201236_data_types__add__server_side',1),
(11,'2017_01_13_000000_add_route_to_menu_items_table',1),
(12,'2017_01_14_005015_create_translations_table',1),
(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),
(14,'2017_03_06_000000_add_controller_to_data_types_table',1),
(15,'2017_04_21_000000_add_order_to_data_rows_table',1),
(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),
(17,'2017_08_05_000000_add_group_to_settings_table',1),
(18,'2017_11_26_013050_add_user_role_relationship',1),
(19,'2017_11_26_015000_create_user_roles_table',1),
(20,'2018_03_11_000000_add_user_settings',1),
(21,'2018_03_14_000000_add_details_to_data_types_table',1),
(22,'2018_03_16_000000_make_settings_value_nullable',1),
(23,'2016_01_01_000000_create_pages_table',2),
(24,'2016_01_01_000000_create_posts_table',2),
(25,'2016_02_15_204651_create_categories_table',2),
(26,'2017_04_11_000000_alter_post_nullable_fields_table',2);

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pages` */

insert  into `pages`(`id`,`author_id`,`title`,`excerpt`,`body`,`image`,`slug`,`meta_description`,`meta_keywords`,`status`,`created_at`,`updated_at`) values 
(1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/page1.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2019-05-16 20:47:19','2019-05-16 20:47:19');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values 
(1,1),
(1,2),
(2,1),
(3,1),
(4,1),
(5,1),
(6,1),
(7,1),
(8,1),
(9,1),
(10,1),
(11,1),
(12,1),
(13,1),
(14,1),
(15,1),
(16,1),
(17,1),
(18,1),
(19,1),
(20,1),
(21,1),
(22,1),
(23,1),
(24,1),
(25,1),
(26,1),
(27,1),
(27,2),
(28,1),
(28,2),
(29,1),
(29,2),
(30,1),
(30,2),
(31,1),
(31,2),
(32,1),
(32,2),
(33,1),
(33,2),
(34,1),
(34,2),
(35,1),
(35,2),
(36,1),
(36,2),
(37,1),
(37,2),
(38,1),
(38,2),
(39,1),
(39,2),
(40,1),
(40,2),
(41,1),
(41,2),
(57,1),
(58,1),
(59,1),
(60,1),
(61,1),
(67,1),
(67,2),
(68,1),
(68,2),
(69,1),
(69,2),
(70,1),
(70,2),
(71,1),
(71,2),
(72,1),
(72,2),
(73,1),
(73,2),
(74,1),
(74,2),
(75,1),
(75,2),
(76,1),
(76,2);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`key`,`table_name`,`created_at`,`updated_at`) values 
(1,'browse_admin',NULL,'2019-05-16 20:42:02','2019-05-16 20:42:02'),
(2,'browse_bread',NULL,'2019-05-16 20:42:02','2019-05-16 20:42:02'),
(3,'browse_database',NULL,'2019-05-16 20:42:02','2019-05-16 20:42:02'),
(4,'browse_media',NULL,'2019-05-16 20:42:03','2019-05-16 20:42:03'),
(5,'browse_compass',NULL,'2019-05-16 20:42:03','2019-05-16 20:42:03'),
(6,'browse_menus','menus','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(7,'read_menus','menus','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(8,'edit_menus','menus','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(9,'add_menus','menus','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(10,'delete_menus','menus','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(11,'browse_roles','roles','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(12,'read_roles','roles','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(13,'edit_roles','roles','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(14,'add_roles','roles','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(15,'delete_roles','roles','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(16,'browse_users','users','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(17,'read_users','users','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(18,'edit_users','users','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(19,'add_users','users','2019-05-16 20:42:03','2019-05-16 20:42:03'),
(20,'delete_users','users','2019-05-16 20:42:04','2019-05-16 20:42:04'),
(21,'browse_settings','settings','2019-05-16 20:42:04','2019-05-16 20:42:04'),
(22,'read_settings','settings','2019-05-16 20:42:04','2019-05-16 20:42:04'),
(23,'edit_settings','settings','2019-05-16 20:42:04','2019-05-16 20:42:04'),
(24,'add_settings','settings','2019-05-16 20:42:04','2019-05-16 20:42:04'),
(25,'delete_settings','settings','2019-05-16 20:42:04','2019-05-16 20:42:04'),
(26,'browse_hooks',NULL,'2019-05-16 20:42:07','2019-05-16 20:42:07'),
(27,'browse_categories','categories','2019-05-16 20:47:14','2019-05-16 20:47:14'),
(28,'read_categories','categories','2019-05-16 20:47:14','2019-05-16 20:47:14'),
(29,'edit_categories','categories','2019-05-16 20:47:14','2019-05-16 20:47:14'),
(30,'add_categories','categories','2019-05-16 20:47:14','2019-05-16 20:47:14'),
(31,'delete_categories','categories','2019-05-16 20:47:14','2019-05-16 20:47:14'),
(32,'browse_posts','posts','2019-05-16 20:47:17','2019-05-16 20:47:17'),
(33,'read_posts','posts','2019-05-16 20:47:17','2019-05-16 20:47:17'),
(34,'edit_posts','posts','2019-05-16 20:47:17','2019-05-16 20:47:17'),
(35,'add_posts','posts','2019-05-16 20:47:17','2019-05-16 20:47:17'),
(36,'delete_posts','posts','2019-05-16 20:47:17','2019-05-16 20:47:17'),
(37,'browse_pages','pages','2019-05-16 20:47:18','2019-05-16 20:47:18'),
(38,'read_pages','pages','2019-05-16 20:47:18','2019-05-16 20:47:18'),
(39,'edit_pages','pages','2019-05-16 20:47:18','2019-05-16 20:47:18'),
(40,'add_pages','pages','2019-05-16 20:47:18','2019-05-16 20:47:18'),
(41,'delete_pages','pages','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(57,'browse_diagnostic','diagnostic','2019-05-16 21:36:27','2019-05-16 21:36:27'),
(58,'read_diagnostic','diagnostic','2019-05-16 21:36:27','2019-05-16 21:36:27'),
(59,'edit_diagnostic','diagnostic','2019-05-16 21:36:27','2019-05-16 21:36:27'),
(60,'add_diagnostic','diagnostic','2019-05-16 21:36:27','2019-05-16 21:36:27'),
(61,'delete_diagnostic','diagnostic','2019-05-16 21:36:27','2019-05-16 21:36:27'),
(67,'browse_certificates','certificates','2019-06-11 17:44:43','2019-06-11 17:44:43'),
(68,'read_certificates','certificates','2019-06-11 17:44:43','2019-06-11 17:44:43'),
(69,'edit_certificates','certificates','2019-06-11 17:44:43','2019-06-11 17:44:43'),
(70,'add_certificates','certificates','2019-06-11 17:44:43','2019-06-11 17:44:43'),
(71,'delete_certificates','certificates','2019-06-11 17:44:43','2019-06-11 17:44:43'),
(72,'browse_topics','topics','2019-06-11 22:09:58','2019-06-11 22:09:58'),
(73,'read_topics','topics','2019-06-11 22:09:58','2019-06-11 22:09:58'),
(74,'edit_topics','topics','2019-06-11 22:09:58','2019-06-11 22:09:58'),
(75,'add_topics','topics','2019-06-11 22:09:58','2019-06-11 22:09:58'),
(76,'delete_topics','topics','2019-06-11 22:09:58','2019-06-11 22:09:58');

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `posts` */

insert  into `posts`(`id`,`author_id`,`category_id`,`title`,`seo_title`,`excerpt`,`body`,`image`,`slug`,`meta_description`,`meta_keywords`,`status`,`featured`,`created_at`,`updated_at`) values 
(1,0,NULL,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/post1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-05-16 20:47:17','2019-05-16 20:47:17'),
(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/post2.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-05-16 20:47:17','2019-05-16 20:47:17'),
(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/post3.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-05-16 20:47:17','2019-05-16 20:47:17'),
(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/post4.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-05-16 20:47:17','2019-05-16 20:47:17');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`display_name`,`created_at`,`updated_at`) values 
(1,'admin','Administrator','2019-05-16 20:42:02','2019-05-16 20:42:02'),
(2,'user','Normal User','2019-05-16 20:42:02','2019-05-16 20:42:02');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`key`,`display_name`,`value`,`details`,`type`,`order`,`group`) values 
(1,'site.title','Site Title','Certificados','','text',1,'Site'),
(2,'site.description','Site Description','Certificados','','text',2,'Site'),
(3,'site.logo','Site Logo','settings/June2019/3CQZa1eNNs6Yk0bY0wmh.png','','image',3,'Site'),
(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),
(5,'admin.bg_image','Admin Background Image','settings/June2019/RJ2GC1FgwhbkBUI9D8rH.jpg','','image',5,'Admin'),
(6,'admin.title','Admin Title','CERTIFICADOS','','text',1,'Admin'),
(7,'admin.description','Admin Description','Generador de Certificados','','text',2,'Admin'),
(8,'admin.loader','Admin Loader','settings/June2019/lcCq0PZNphMoE28qgDQx.png','','image',3,'Admin'),
(9,'admin.icon_image','Admin Icon Image','settings/June2019/bDBLNxGLTPLLju9rQ7Kq.png','','image',4,'Admin'),
(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','908340655841-s4v2qm637ufq7l0fqqqtp8sqpbq9e6vd.apps.googleusercontent.com','','text',1,'Admin');

/*Table structure for table `topics` */

DROP TABLE IF EXISTS `topics`;

CREATE TABLE `topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_padding_top` int(11) DEFAULT '0',
  `name_padding_left` int(11) DEFAULT '0',
  `identified_padding_top` int(11) DEFAULT '0',
  `identiffied_padding_left` int(11) DEFAULT '0',
  `orientation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'HORIZONTAL',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `topics` */

insert  into `topics`(`id`,`name`,`observation`,`created_at`,`updated_at`,`deleted_at`,`image`,`name_padding_top`,`name_padding_left`,`identified_padding_top`,`identiffied_padding_left`,`orientation`) values 
(1,'Conflictos y dilemas éticos en la investigación.','Conversatorios','2019-06-11 22:17:00','2019-06-15 01:38:48',NULL,'topics/June2019/CnnuQAFUxIFowZtJIXyr.png',375,0,20,70,'VERTICAL'),
(3,'Primer Encuentro Abierto de Permacultura',NULL,'2019-11-14 23:24:00','2019-11-26 15:49:01',NULL,'topics/November2019/LPeaTIojsJx8OHfu7mhq.jpg',250,NULL,150,45,'HORIZONTAL'),
(4,'Política Pública de Gobierno Abierto',NULL,'2019-11-14 23:25:00','2019-11-26 16:26:23',NULL,'topics/November2019/0xomxA8c16zg5WhrhO2u.jpg',315,NULL,128,30,'HORIZONTAL');

/*Table structure for table `translations` */

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `translations` */

insert  into `translations`(`id`,`table_name`,`column_name`,`foreign_key`,`locale`,`value`,`created_at`,`updated_at`) values 
(1,'data_types','display_name_singular',5,'pt','Post','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(2,'data_types','display_name_singular',6,'pt','Página','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(3,'data_types','display_name_singular',1,'pt','Utilizador','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(4,'data_types','display_name_singular',4,'pt','Categoria','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(5,'data_types','display_name_singular',2,'pt','Menu','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(6,'data_types','display_name_singular',3,'pt','Função','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(7,'data_types','display_name_plural',5,'pt','Posts','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(8,'data_types','display_name_plural',6,'pt','Páginas','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(9,'data_types','display_name_plural',1,'pt','Utilizadores','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(10,'data_types','display_name_plural',4,'pt','Categorias','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(11,'data_types','display_name_plural',2,'pt','Menus','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(12,'data_types','display_name_plural',3,'pt','Funções','2019-05-16 20:47:19','2019-05-16 20:47:19'),
(13,'categories','slug',1,'pt','categoria-1','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(14,'categories','name',1,'pt','Categoria 1','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(15,'categories','slug',2,'pt','categoria-2','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(16,'categories','name',2,'pt','Categoria 2','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(17,'pages','title',1,'pt','Olá Mundo','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(18,'pages','slug',1,'pt','ola-mundo','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(20,'menu_items','title',1,'pt','Painel de Controle','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(21,'menu_items','title',2,'pt','Media','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(22,'menu_items','title',13,'pt','Publicações','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(23,'menu_items','title',3,'pt','Utilizadores','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(24,'menu_items','title',12,'pt','Categorias','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(25,'menu_items','title',14,'pt','Páginas','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(26,'menu_items','title',4,'pt','Funções','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(27,'menu_items','title',5,'pt','Ferramentas','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(28,'menu_items','title',6,'pt','Menus','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(29,'menu_items','title',7,'pt','Base de dados','2019-05-16 20:47:20','2019-05-16 20:47:20'),
(30,'menu_items','title',10,'pt','Configurações','2019-05-16 20:47:20','2019-05-16 20:47:20');

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_roles` */

insert  into `user_roles`(`user_id`,`role_id`) values 
(4,1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`role_id`,`name`,`email`,`avatar`,`email_verified_at`,`password`,`remember_token`,`settings`,`created_at`,`updated_at`) values 
(1,1,'Admin','admin@admin.com','users/June2019/0RQaF5YJjoKHbYvNdHw8.png',NULL,'$2y$10$drTyuhAMvBaiz/q7J4rvTuZPVDTJRazDfryJ7sRRDZ5UP8fJ5Rvvm','f6IPLbuUpqZ7xvmbhnju8Tr5S3msW3Veqa4TqrUPp7JGS3GrBSPZpcF9tnEW','{\"locale\":\"es\"}','2019-05-16 20:47:15','2019-06-13 15:04:47'),
(2,1,'Oscar Zambrano','oscarzambranoc@gmail.com','users/June2019/Bk0ytR3XvqixCLmqkdGH.png',NULL,'$2y$10$Omlw3QE56ga0MFXYk7HZR.S4Z3zbZHXzMB.8KLg0FQCme.j.18fga',NULL,'{\"locale\":\"es\"}','2019-06-13 19:56:40','2019-06-13 20:29:00'),
(3,1,'Catalina Arturo','catalinam.arturo@gmail.com','users/June2019/XmyKmaz6osxjfUyPsMhU.png',NULL,'$2y$10$SmdBC7MdeFLfZ6y72JXnMObxRdOD8LORZlOnZPCr3U4CLTN3ev3sC','tUpN57Fs6pIQLgUGtYjqkOYD901t4VAZsmpEPRSdnk7ovfLF6WWwzKxzwsEi','{\"locale\":\"es\"}','2019-06-13 20:19:09','2019-06-13 20:29:14'),
(4,1,'Angela Caicedo','angelacaicedo@narino.gov.co','users/default.png',NULL,'$2y$10$ZoeXjWMoNtbdiQ8zixsGP.5k.DTVJdlcleifKR3W2/jdpa/MVlqVu','stoV9uLvF8nEVNYTAYXAgC2QK04ehRvOtrmy4WII3tIHfYhVwDQM4Le5IlFF','{\"locale\":\"es\"}','2019-11-29 18:39:30','2019-11-29 18:39:30');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
