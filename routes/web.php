<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AdminController@show')->name('admin.show');

Route::name('print')->get('/imprimir', 'GeneradorController@imprimir');
Route::get('certificate', 'GeneradorController@index')->name('certificate');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
