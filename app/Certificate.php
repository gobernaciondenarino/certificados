<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    //

    public function topic(){
        return $this->belongsTo('App\Topic');
    }

    public function scopeSearch($query, $identified, $topic){

        return $query
                ->where('identified', '=', $identified)
                ->where('topic_id', '=', $topic);

    }
}
