<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <style>

        body{

            background-image: url({{ Voyager::image($certificate->topic->image)}});
            background-position: center;
            background-repeat: no-repeat;
            background-size: 100%;
            width: 100%;
            height: 100%;
            padding: 0px;
            margin: 0px;
        }
        .contenido{
        font-size: 20px;
        }
        #primero{
        font-family: Arial, Helvetica, sans-serif;
        text-align: center;
        padding-top: {{ $certificate->topic->name_padding_top }}px;
        }
        #segundo{
        font-family: Arial, Helvetica, sans-serif;
        text-align: center;
        padding-bottom: {{ $certificate->topic->identified_padding_top }}px;
        padding-left: {{ $certificate->topic->identiffied_padding_left }}px;
        }

    </style>
    </head>
    <body>
        <div class="contenido">
            <p id="primero">{{ $certificate->name }} </p>
            <p id="segundo">{{ $certificate->identified }} </p>
        </div>
    </body>
</html>


