<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Certificados | Gobernación de Nariño</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ asset('vendor/fonts/material-icon/css/material-design-iconic-font.min.css') }}">

    <!-- Main css -->
    <link rel="stylesheet" href="{{ asset('vendor/css/style.css') }}">
</head>
<body>

    <div class="main">

        <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
            <div class="container">
                <div class="signup-content">
                    <form id="signup-form" class="signup-form" action="{{ route('certificate') }}">
                        @csrf('GET')
                        <h2 class="form-title">CERTIFICADOS DE ASISTENCIA</h2>
                        <div class="form-group">
                            <label for="name">Documento de Identificación</label>
                            <input type="text" class="form-input" name="identified" id="identified" required="" placeholder="Digite Su documento de Identidad"/>
                        </div>
                        <div class="form-group">
                            <label for="name">Tema del Certificado</label>
                            <select class="form-input" name="topic_id" id="topic_id" required="">
                                    <option value="" disabled="" selected="">-- Escoja el tema de su Certificado</option>
                                    @foreach ($topics as $topic)
                                        <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                                    @endforeach
                                </select>
                        </div>
                        {{-- <div class="form-group">
                            <input type="checkbox" name="agree-term" id="agree-term" required="" class="agree-term" />
                            <label for="agree-term" class="label-agree-term"><span><span></span></span>Acepta las condiciones del documento ?  <a href="#" class="term-service">Condiciones</a></label>
                        </div> --}}
                        <div class="form-group">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Generar Certificado"/>
                        </div>
                    </form>
                </div>
                <div class="signup-content">
                    <img src="{{ asset('vendor/images/Certificado-01-pie.jpg') }}" alt="Patrocinadores" width="1000" height="500">
                </div>
            </div>
        </section>

    </div>

    @include('sweetalert::alert')
    <!-- JS -->
    <script src="{{ asset('vendor/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/js/main.js') }}"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
